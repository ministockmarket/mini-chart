## Mini-Stock-Market Chart Service

## How to build Docker image
If you want to rebuild app for testing in local environment use this command: 
```bash
./gradlew dockerBuild
```

## How to start app inside Docker container
App expects that you have running mini-feed service on localhost:50052 or address specified in environment variable MINI_FEED_HOST.

Before you start app you need to create network if it doesn't exist:
```bash
docker network create ministockmarket
```
Then you can start mini-feed service using this command with image from Docker Hub:
```bash
docker run --name mini-feed --rm -p 50052:50051 --network ministockmarket mbaluch/mini-feed
```
Then you can start app using this command with image built in previous step or pulled from Docker Hub:
```bash
docker run --name mini-chart --rm -p 50051:50051 --network ministockmarket -e MINI_FEED_HOST=mini-feed mbaluch/mini-chart
```
After that you can connect to apps using grpcurl or other gRPC client, mini-feed is available on `localhost:50052` and mini-chart on `localhost:50051`.

Cleanup:
```bash
docker network rm ministockmarket
```

---

## How to connect using grpcurl
### SubscribeCandles
Returns stream of candles for specified instrument and candle type.
```bash
grpcurl -plaintext -proto ./src/main/proto/mini-chart.proto -d '{"type": "ONE_MINUTE", "instrument": "Mock1"}' localhost:50051 pl.baluch.ministock.minichart.MiniChartService/SubscribeCandles
```