package pl.baluch.ministock.minichart.data;

import java.util.Iterator;
import java.util.LinkedList;

public class CandleBuffer implements Iterable<Candle> {

    private final int size;
    private final LinkedList<Candle> candles;

    public CandleBuffer(int size) {
        this.size = size;
        candles = new LinkedList<>();
    }

    public void add(Candle candle) {
        if (candles.size() == size) {
            candles.removeFirst();
        }
        candles.add(candle);
    }

    public int size() {
        return candles.size();
    }

    @Override
    public Iterator<Candle> iterator() {
        return candles.iterator();
    }
}
