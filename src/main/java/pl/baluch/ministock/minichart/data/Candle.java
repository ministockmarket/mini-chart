package pl.baluch.ministock.minichart.data;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.ToString;
import lombok.experimental.FieldDefaults;

@Getter
@ToString
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class Candle {
    final double open;
    double high;
    double low;
    double close;
    final long start;
    final long end;

    public Candle(double price, long start, long end) {
        this.open = price;
        this.high = price;
        this.low = price;
        this.close = price;
        this.start = start;
        this.end = end;
    }

    public void processNewPrice(double price) {
        if (price > high) {
            high = price;
        }
        if (price < low) {
            low = price;
        }
        close = price;
    }

}
