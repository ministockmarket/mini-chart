package pl.baluch.ministock.minichart;

import io.grpc.stub.StreamObserver;
import jakarta.inject.Singleton;
import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;
import pl.baluch.ministock.minichart.storage.CandleStorage;

@Singleton
@RequiredArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE, makeFinal = true)
public class GrpcEndpoint extends MiniChartServiceGrpc.MiniChartServiceImplBase {

    CandleStorage candleStorage;

    @Override
    public void subscribeCandles(SubscribeCandlesRequest request, StreamObserver<SubscribeCandlesResponse> responseObserver) {
        String instrument = request.getInstrument();
        CandleType type = request.getType();
        candleStorage.getAndSubscribeCandles(instrument, type)
                .map(candle -> SubscribeCandlesResponse.newBuilder()
                        .setOpenPrice(candle.getOpen())
                        .setHighPrice(candle.getHigh())
                        .setLowPrice(candle.getLow())
                        .setClosePrice(candle.getClose())
                        .setStartTimestamp(candle.getStart())
                        .setEndTimestamp(candle.getEnd())
                        .build())
                .subscribe(responseObserver::onNext, responseObserver::onError, responseObserver::onCompleted);
    }
}
