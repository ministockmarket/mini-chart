package pl.baluch.ministock.minichart;

import io.grpc.ManagedChannel;
import io.micronaut.context.annotation.Factory;
import io.micronaut.grpc.annotation.GrpcChannel;
import jakarta.inject.Singleton;
import pl.baluch.ministock.minifeed.MiniFeedServiceGrpc;

@Factory
public class GrpcChannelFactory {
    @Singleton
    MiniFeedServiceGrpc.MiniFeedServiceStub miniFeedService(@GrpcChannel("mini-feed") ManagedChannel channel) {
        return MiniFeedServiceGrpc.newStub(channel);
    }
}
