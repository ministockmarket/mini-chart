package pl.baluch.ministock.minichart.utils;

import pl.baluch.ministock.minichart.CandleType;

import java.util.concurrent.TimeUnit;

public class CandleTimeConverter {
    public static final long MINUTE = TimeUnit.MINUTES.toMillis(1);
    public static final long HOUR = TimeUnit.HOURS.toMillis(1);
    public static final long DAY = TimeUnit.DAYS.toMillis(1);

    public static long getCandleTypeMillis(CandleType type) {
        return switch (type) {
            case ONE_MINUTE -> MINUTE;
            case FIVE_MINUTES -> 5 * MINUTE;
            case FIFTEEN_MINUTES -> 15 * MINUTE;
            case THIRTY_MINUTES -> 30 * MINUTE;
            case ONE_HOUR -> HOUR;
            case FOUR_HOURS -> 4 * HOUR;
            case ONE_DAY -> DAY;
            case UNRECOGNIZED -> throw new IllegalArgumentException("Unrecognized candle type");
        };
    }

    public static long getStart(CandleType type, long timestamp) {
        return timestamp - timestamp % getCandleTypeMillis(type);
    }

    public static long getEnd(CandleType type, long quoteTimestamp) {
        return getStart(type, quoteTimestamp) + getCandleTypeMillis(type);
    }
}
