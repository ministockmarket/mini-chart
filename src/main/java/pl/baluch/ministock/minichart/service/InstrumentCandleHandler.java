package pl.baluch.ministock.minichart.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import pl.baluch.ministock.minichart.CandleType;
import pl.baluch.ministock.minichart.data.Candle;
import pl.baluch.ministock.minichart.storage.CandleStorage;
import pl.baluch.ministock.minichart.utils.CandleTimeConverter;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class InstrumentCandleHandler {

    private final CandleStorage candleStorage;
    private final String instrumentName;
    private final Map<CandleType, Candle> activeCandles = new HashMap<>();

    public void processNewPrice(double price, long quoteTimestamp) {
        for (CandleType type : CandleType.values()) {
            if (type == CandleType.UNRECOGNIZED) {
                continue;
            }
            if (!activeCandles.containsKey(type)) {
                createCandle(type, price, quoteTimestamp);
            }
            activeCandles.get(type).processNewPrice(price);
        }
    }

    private void createCandle(CandleType type, double price, long quoteTimestamp) {
        long start = CandleTimeConverter.getStart(type, quoteTimestamp);
        long end = CandleTimeConverter.getEnd(type, quoteTimestamp);
        activeCandles.put(type, new Candle(price, start, end));
    }

    public void closeCandles() {
        long now = System.currentTimeMillis();
        for (CandleType type : CandleType.values()) {
            if (type == CandleType.UNRECOGNIZED || !activeCandles.containsKey(type)) {
                continue;
            }
            Candle candle = activeCandles.get(type);
            if (candle.getEnd() <= now) {
                candleStorage.publishCandle(instrumentName, type, candle);
                createCandle(type, candle.getClose(), candle.getEnd());
            }
        }
    }
}