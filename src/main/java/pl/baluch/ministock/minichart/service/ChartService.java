package pl.baluch.ministock.minichart.service;

import io.grpc.stub.StreamObserver;
import io.micronaut.context.annotation.Context;
import io.micronaut.scheduling.annotation.Scheduled;
import lombok.extern.slf4j.Slf4j;
import pl.baluch.ministock.minichart.storage.CandleStorage;
import pl.baluch.ministock.minifeed.FeedResponse;
import pl.baluch.ministock.minifeed.MiniFeedServiceGrpc;
import pl.baluch.ministock.minifeed.SubscribeAllRequest;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Context
public class ChartService {

    private final CandleStorage candleStorage;
    private final MiniFeedServiceGrpc.MiniFeedServiceStub miniFeedService;
    private final Map<String, InstrumentCandleHandler> candleHandlers = new HashMap<>();
    private long counter = 0;

    public ChartService(CandleStorage candleStorage, MiniFeedServiceGrpc.MiniFeedServiceStub miniFeedService) {
        this.candleStorage = candleStorage;
        this.miniFeedService = miniFeedService;
        subscribe();
    }

    @Scheduled(cron = "0 * * * * ?")
    public void closeCandles() {
        log.info("Closing candles, tick counter: {}", counter);
        for (InstrumentCandleHandler value : candleHandlers.values()) {
            value.closeCandles();
        }
    }

    private void subscribe() {
        log.info("Connecting to mini-feed...");
        miniFeedService.subscribeAll(SubscribeAllRequest.newBuilder().build(), new StreamObserver<>() {
            @Override
            public void onNext(FeedResponse value) {
                processFeed(value);
            }

            @Override
            public void onError(Throwable t) {
                log.error("Exception occur on subscription to mini-feed:", t);
            }

            @Override
            public void onCompleted() {
                log.info("Connection to mini-feed has been closed");
            }
        });
    }

    private void processFeed(FeedResponse response) {
        counter++;
        getInstrumentCandleHandler(response.getName())
                .processNewPrice(response.getPrice(), response.getQuoteTimestamp());
    }

    private InstrumentCandleHandler getInstrumentCandleHandler(String instrumentName) {
        if (!candleHandlers.containsKey(instrumentName)) {
            candleHandlers.put(instrumentName, new InstrumentCandleHandler(candleStorage, instrumentName));
        }
        return candleHandlers.get(instrumentName);
    }
}
