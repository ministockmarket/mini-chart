package pl.baluch.ministock.minichart.storage;

import jakarta.inject.Singleton;
import lombok.extern.slf4j.Slf4j;
import pl.baluch.ministock.minichart.CandleType;
import pl.baluch.ministock.minichart.data.Candle;
import pl.baluch.ministock.minichart.data.CandleBuffer;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;

import java.util.HashMap;
import java.util.Map;

@Slf4j
@Singleton
public class CandleStorage {
    private final Map<CandleKey, CandleBuffer> candles = new HashMap<>();
    private final Map<CandleKey, Sinks.Many<Candle>> candleSinks = new HashMap<>();

    public Flux<Candle> getAndSubscribeCandles(String instrumentName, CandleType type) {
        CandleKey key = new CandleKey(instrumentName, type);
        Flux<Candle> sinkFlux = getSink(key).asFlux();
        if (candles.containsKey(key)) {
            return Flux.fromIterable(candles.get(key))
                    .concatWith(sinkFlux);
        }
        return sinkFlux;
    }

    private Sinks.Many<Candle> getSink(CandleKey key) {
        if (!candleSinks.containsKey(key)) {
            candleSinks.put(key, Sinks.many().multicast().directBestEffort());
        }
        return candleSinks.get(key);
    }

    public void publishCandle(String instrumentName, CandleType type, Candle candle) {
        CandleKey key = new CandleKey(instrumentName, type);
        saveCandle(key, candle);
        getSink(key).tryEmitNext(candle);
        log.info("Candle published for instrument {}, type {}: {}", instrumentName, type.name(), candle);
    }

    private void saveCandle(CandleKey key, Candle candle) {
        if (!candles.containsKey(key)) {
            candles.put(key, new CandleBuffer(100));
        }
        candles.get(key).add(candle);
    }
}
