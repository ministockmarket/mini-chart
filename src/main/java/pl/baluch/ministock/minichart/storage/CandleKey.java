package pl.baluch.ministock.minichart.storage;

import pl.baluch.ministock.minichart.CandleType;

public record CandleKey(String instrumentName, CandleType type) {
}
