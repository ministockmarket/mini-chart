# Kubernetes deployment
## How to deploy app to kubernetes cluster:
Prepare namespace using instructions from [infrastructure repository](https://gitlab.com/ministockmarket/infrastructure)

Deploy configmap as described in [config README](./config/README.md)

Pull helm dependencies:
```bash
$ helm dependency build ./charts
```
or update dependencies:
```bash
$ helm dependency update ./charts
```

Deploy app using `helm` command:
```bash
$ helm install mini-chart ./charts -n ministockmarket
```

## Test deployed app
List all pods in `ministockmarket` namespace:
```bash
kubectl get pods -n ministockmarket
```
Expected output:
```text
NAME                         READY   STATUS    RESTARTS   AGE
mini-feed-5b7459c76f-lfsns   1/1     Running   0          9h
mini-chart-57fddbb65-lb68c   1/1     Running   0          4s
```
Copy mini-chart pod name from output and use it in following commands

Check app logs:
```bash
kubectl logs mini-chart-57fddbb65-lb68c -n ministockmarket
```
Forward pod's port to host:
```bash
kubectl port-forward pod/mini-chart-57fddbb65-lb68c 50051:50051 -n ministockmarket
```
And then check grpc services as usual using localhost:50051 as host.


Use domain specified in `./charts/values.yaml` to access app from outside cluster.
```bash
grpcurl -authority ministockmarket.pl -insecure -proto ./src/main/proto/mini-chart.proto -d '{"type":"ONE_MINUTE","instrument":"ConfigMock1"}' <cluster ip address>:443 pl.baluch.ministock.minichart.MiniChartService/SubscribeCandles
```