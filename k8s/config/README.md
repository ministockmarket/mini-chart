# Application Configuration
This directory contains the configuration files for the application.

## How to install configuration
```bash
kubectl create configmap mini-chart --from-file=mini-chart.yml -n ministockmarket
```

## How to update configuration
```bash
kubectl create configmap mini-chart --from-file=mini-chart.yml -n ministockmarket -o yaml --dry-run=client | kubectl replace -f -
```

## How to delete configuration
```bash
kubectl delete configmap mini-chart -n ministockmarket
```
